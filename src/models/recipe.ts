import type { DocumentData } from 'firebase/firestore';

export default class Recipe {
  name: string;
  slug: string;
  created: Date;
  views: number;

  constructor(data: Record<string, unknown> | DocumentData) {
    this.name = data.name || '';
    this.slug = data.slug || '';
    this.views = data.views || 0;
    // Firebase timestamps is an object with 'seconds' in it
    this.created = data.created?.seconds
      ? new Date(data.created?.seconds * 1000)
      : data.created instanceof Date
      ? data.created
      : new Date();
  }
}
