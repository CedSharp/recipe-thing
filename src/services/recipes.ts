import Recipe from '../models/recipe';
import { db } from '../firebase-admin';

class Recipes {
  protected recipes: Recipe[] = [];

  protected async retrieveRecipes() {
    const snapshot = await db.collection('recipes').get();
    const recipes = snapshot?.docs?.map((d) => d.data()) || [];
    this.recipes = recipes.map((d) => new Recipe(d));
  }

  async all() {
    if (this.recipes.length === 0) await this.retrieveRecipes();
  }

  async latest() {
    await this.all();
    const latest = [...this.recipes];
    // @ts-ignore because Date is actually comparable
    latest.sort((a, b) => b.created - a.created);
    return latest;
  }

  async popular() {
    await this.all();
    const popular = [...this.recipes];
    popular.sort((a, b) => b.views - a.views);
    return popular;
  }
}

export default new Recipes();
