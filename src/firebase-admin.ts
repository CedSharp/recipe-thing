import { dirname, join, resolve } from 'path';
import { fileURLToPath } from 'url';
import { firebaseConfig } from './admin/lib/firebase';
import admin from 'firebase-admin/app';
import { getFirestore } from 'firebase-admin/firestore';

const dir = dirname(fileURLToPath(import.meta.url));
const serviceAccount = resolve(join(dir, 'firebase-admin-sdk.json'));

const app = admin.initializeApp(
  {
    ...firebaseConfig,
    credential: admin.cert(serviceAccount)
  },
  'astro'
);

export const db = getFirestore(app);
