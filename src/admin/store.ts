import { atom } from 'nanostores';

export const displayName = atom('');
export const logged = atom(false);
export const isAdmin = atom(false);
export const path = atom('login');
