import { initializeApp } from 'firebase/app';
import * as Firestore from 'firebase/firestore';
import * as Auth from 'firebase/auth';

export const firebaseConfig = {
  apiKey: 'AIzaSyAc_QUchbYBqHcS03Xe4mQHMIQLj5oOogs',
  authDomain: 'recipe-dubois.firebaseapp.com',
  projectId: 'recipe-dubois',
  storageBucket: 'recipe-dubois.appspot.com',
  messagingSenderId: '903094993801',
  appId: '1:903094993801:web:584b4f94459402c7661758'
};

export const app = initializeApp(firebaseConfig);
export const db = Firestore.getFirestore(app);
export const auth = Auth.getAuth(app);
