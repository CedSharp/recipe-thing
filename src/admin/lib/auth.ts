import { app, auth, db } from './firebase';
import { getDoc, collection, doc } from 'firebase/firestore';
import * as AppCheck from 'firebase/app-check';
import {
  setPersistence,
  signInWithEmailAndPassword,
  signInWithPopup,
  signOut,
  browserSessionPersistence,
  onAuthStateChanged,
  User,
  UserCredential,
  GoogleAuthProvider,
  FacebookAuthProvider
} from 'firebase/auth';
import { path, logged, displayName, isAdmin } from '../store';

class Auth {
  private resolve!: (val: User | null) => void;
  private promise = new Promise((res) => (this.resolve = res));
  private user: null | User = null;

  private listeners: Array<(user: null | User) => void> = [];

  constructor() {
    void this.init();
  }

  private async init() {
    AppCheck.initializeAppCheck(app, {
      provider: new AppCheck.ReCaptchaV3Provider(
        '6LcHxfojAAAAAGsTT7O_WwpyTqFUI47YvLtge7wl'
      )
    });

    setPersistence(auth, browserSessionPersistence).catch(console.error);

    let firstTime = true;
    onAuthStateChanged(auth, (user: User | null) => {
      this.user = user;
      logged.set(this.user !== null);
      displayName.set(
        this.user === null ? '' : this.user.displayName || this.user.email || ''
      );

      if (firstTime) {
        firstTime = false;

        if (user !== null)
          try {
            Promise.all([
              getDoc(doc(collection(db, 'allowed'), user.email as string)),
              getDoc(doc(collection(db, 'admins'), user.email as string))
            ]).then(([_, admins]) => {
              isAdmin.set(admins.exists());
              this.resolve(user);
            });
          } catch (err) {
            this.user = null;
            logged.set(false);
            this.resolve(null);
            void signOut(auth);
          }
        else this.resolve(user);
      }
    });
  }

  listen(listener: (user: null | User) => void) {
    this.listeners.push(listener);
  }

  ready() {
    return this.promise;
  }

  private async onSignedIn(creds: UserCredential): Promise<string> {
    // The following will throw an error if user does not exists
    try {
      const [_, admins] = await Promise.all([
        getDoc(doc(collection(db, 'allowed'), creds.user.email as string)),
        getDoc(doc(collection(db, 'admins'), creds.user.email as string))
      ]);
      isAdmin.set(admins.exists());
    } catch (err) {
      void signOut(auth);
      throw new Error('unauthorized');
    }

    this.user = creds.user as User;
    for (const listener of this.listeners) listener(this.user);
    return '';
  }

  async signInWithEmail(email: string, password: string): Promise<string> {
    await this.promise;
    return new Promise((resolve) => {
      signInWithEmailAndPassword(auth, email, password)
        .then((creds) => this.onSignedIn(creds))
        .then(resolve)
        .catch((err) => {
          console.error(err);
          signOut(auth).then(null).catch(null);
          resolve(err.message);
        });
    });
  }

  async signInWithGoogle(): Promise<string> {
    await this.promise;
    return new Promise((resolve) => {
      signInWithPopup(auth, new GoogleAuthProvider())
        .then((creds) => this.onSignedIn(creds))
        .then(resolve)
        .catch((err) => {
          console.error(err);
          resolve(err.message);
        });
    });
  }

  async signInWithFacebook(): Promise<string> {
    await this.promise;
    return new Promise((resolve) => {
      signInWithPopup(auth, new FacebookAuthProvider())
        .then((creds) => this.onSignedIn(creds))
        .then(resolve)
        .catch((err) => {
          console.error(err);
          resolve(err.message);
        });
    });
  }

  async signOut() {
    await this.promise;
    await signOut(auth);
    this.user = null;
    logged.set(false);
    for (const listener of this.listeners) listener(this.user);
    path.set('login');
  }

  async getUser(): Promise<null | User> {
    await this.promise;
    return this.user;
  }
}

let authService: Auth;
export const getAuth = () => {
  if (!authService) authService = new Auth();
  return authService;
};
