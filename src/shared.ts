export interface Resource {
  name: string;
  slug: string;
}

export interface Recipe extends Resource {}

export interface Category extends Resource {}
