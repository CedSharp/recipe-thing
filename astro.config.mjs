import { defineConfig } from "astro/config";

// Integrations
import astroI18next from 'astro-i18next';
import vue from "@astrojs/vue";

export default defineConfig({
  integrations: [
    astroI18next(),
    vue({ appEntrypoint: "/src/entry.ts" }),
  ],
});
