# Recipe for Dubois family

This static website has 2 parts:

- The website itself, which is static and hosted on firebase with
  some rules that prevents unauthorized users to access the website.

- The administration, which is also static and connects to firestore
  to update the values, then triggering github action to start.

## Prerequesit

Since this project connects to firebase, you'll need 2 things:

- The configuration object generated in the console for the admin
- The service account downloaded in the console for astro to generate the website.
